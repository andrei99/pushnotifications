package com.pushnotifications.mobile.pushnotifications.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.pushnotifications.mobile.pushnotifications.R;
import com.pushnotifications.mobile.pushnotifications.services.RegistrationIntentService;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button logTokenButton = (Button) findViewById(R.id.logTokenButton);
        logTokenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get token
                String token = FirebaseInstanceId.getInstance().getToken();
                sendRegistrationToServer(token);
                // Log and toast
                String msg = token;
                Log.d(TAG, msg);
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendRegistrationToServer(String token) {
        //Implement this method if you want to store the token on your server
        Intent msgIntent = new Intent(this, RegistrationIntentService.class);
        msgIntent.putExtra(RegistrationIntentService.TOKEN, token);
        startService(msgIntent);
    }
}
