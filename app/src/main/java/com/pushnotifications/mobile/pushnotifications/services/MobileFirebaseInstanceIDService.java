package com.pushnotifications.mobile.pushnotifications.services;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by andrei.boleac on 06/11/16.
 */

public class MobileFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MobileFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        //Get hold of the registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log the token
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        //Implement this method if you want to store the token on your server
        Intent msgIntent = new Intent(this, RegistrationIntentService.class);
        msgIntent.putExtra(RegistrationIntentService.TOKEN, token);
        startService(msgIntent);
    }


}
