package com.pushnotifications.mobile.pushnotifications.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.pushnotifications.mobile.pushnotifications.R;
import com.pushnotifications.mobile.pushnotifications.models.Video;
import com.pushnotifications.mobile.pushnotifications.models.VideoContainer;

/**
 * Created by andrei.boleac on 10/05/2017.
 */

public class VideoDetailsActivity extends AppCompatActivity {
    public static final String VIDEO = "video";

    private TextView videoTitle;
    private TextView videoDescription;
    private ImageView videoImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_details);
        videoTitle = (TextView) findViewById(R.id.vide_title);
        videoDescription = (TextView) findViewById(R.id.video_description);
        videoImage = (ImageView) findViewById(R.id.video_image);
        onNewIntent(getIntent());
    }

    @Override
    public void onNewIntent(Intent intent) {
        Bundle extras = intent.getBundleExtra(VIDEO);
        if (extras != null) {
            if (extras.containsKey(VIDEO)) {

                // extract the extra-data in the Notification
                String jsonVideo = extras.getString(VIDEO);
                setMedataData(Video.parseVideo(jsonVideo));
            }
        }
    }

    private void setMedataData(Video video) {
        videoTitle.setText(video.getVideoTitle());
        videoDescription.setText(video.getVideoDescription());
        Glide.with(this).load(video.getImage()).into(videoImage);
    }

}
