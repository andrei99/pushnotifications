package com.pushnotifications.mobile.pushnotifications.models;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.annotations.SerializedName;

/**
 * Created by andrei.boleac on 15/05/2017.
 */

public class Video {

    public static final String VIDEO = "video";

    @SerializedName("video_id")
    private String id;
    @SerializedName("video_description")
    private String videoDescription;
    @SerializedName("video_title")
    private String videoTitle;
    private String image;


    public String getId() {
        return id;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public String getImage() {
        return image;
    }

    public static Video parseVideo(String jsonVideo) {
        Gson gson = new Gson();
        JsonArray jsonArray = gson.fromJson(jsonVideo, JsonArray.class);
        VideoContainer videoContainer = gson.fromJson(jsonArray.get(0), VideoContainer.class);
        return videoContainer.getFields();
    }
}
